DESCRIPTION
===========

This is package for using in python web server, developed to
prosessing text classification. This package is providing
access to different types of classifiers, learning tasks, etc.
