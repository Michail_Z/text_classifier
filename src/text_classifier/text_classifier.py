import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import hashlib as hl


class TextClassifier:

    # Константы
    SUCCESS = 0
    BUKET_EXISTED = 1
    TYPE_INCORRECT = 2
    FILE_NOTFOUND = -1
    DONE = 0
    RUNNING = 1
    BUKET_ISCLEAR = 2
    # Переменные
    model = {}
    Train = 0
    categories = []
    Prediction = {}
    answer = {}
    vect = ""
    dataFrame = ""

    def __init__(self):
        self.Train = self.BUKET_ISCLEAR
        self.vect = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
                                    analyzer='word')

    """
    Функция Create_buket отвечает за выбор типа классификатора и создание моде-
    ли. Результат работы функции - словарь model, содержащий в себе следующие
    пары:
        ID - цифровой идентификатор (берется из аргумента BUKET_ID)
        CLF - тип классификатора (берется из аргумента TYPE).
    """

    def Create_buket(self, BUKET_ID, TYPE):

        if BUKET_ID < 0:
            return self.BUKET_EXISTED

        if TYPE == 'LR':  # Классификатор по методу логистической регрессии
            from sklearn.linear_model import LogisticRegression
            self.model[BUKET_ID] = LogisticRegression()

        elif TYPE == 'MNB':  # Мультиномиальный наивный байес
            from sklearn.naive_bayes import MultinomialNB
            self.model[BUKET_ID] = MultinomialNB()

        elif TYPE == 'GNB':  # Наивный Байес на Гауссовом распределении
            from sklearn.naive_bayes import GaussianNB
            self.model[BUKET_ID] = GaussianNB()

        elif TYPE == 'KNN':  # Классификация по методу ближайших соседей
            from sklearn.neighbors import KNeighborsClassifier
            self.model[BUKET_ID] = KNeighborsClassifier()

        elif TYPE == 'CRT':  # Классификация по методу деревьев решений
            from sklearn.neighbors import DecisionTreeClassifier
            self.model[BUKET_ID] = DecisionTreeClassifier()

        elif TYPE == 'SVC':  # Классификация по методу опорных векторов
            from sklearn.neighbors import SVC
            self.model[BUKET_ID] = SVC()

        else:
            return self.TYPE_INCORRECT

        return self.SUCCESS

    def Train_the_model(self, BUKET_ID, URL_FILES):

        # Предварительно проверяем наличие файла по указанному пути
        from os import path
        if path.exists(URL_FILES) == False:
            self.Train = self.FILE_NOTFOUND
            return

        # Считываем файл и проводим предварительную обработку
        self.Train = self.RUNNING
        self.dataFrame = pd.read_csv(URL_FILES)

        # Преобразуем категории в вид, понятный классификатору
        self.dataFrame['cat_no'] = pd.Categorical(pd.factorize(
            self.dataFrame.state_id)[0])

        # Выделяем категории в отдельный массив без дубликатов
        self.categories = self.dataFrame.state_id.unique()

        # Токенизинуем текст
        X = self.vect.fit_transform(self.dataFrame.words)

        # Запускаем процесс обучения модели
        self.model[BUKET_ID].fit(X, self.dataFrame.cat_no)
        self.Train = self.DONE
        return

    def Train_is_done(self, BUKET_ID):
        return self.Train

    def Train_log(self, BUKET_ID):
        pass

    def Test(self, BUKET_ID):
        pass

    def Predict(self, BUKET_ID, TEXT):
        new = [TEXT]
        X = (self.vect.transform(new))
        result = self.model[BUKET_ID].predict(X)
        # А здесь надо как-то собрать данные, так, чтобы сохранился текст,
        # результат
        for doc, category in zip(new, result):
            self.answer['text'] = TEXT
            self.answer['categoty'] = self.categories[category]
        # Расчитаем TOKEN по TEXT по алгоритму MD5
        TOKEN = hl.sha224(bytes(TEXT, encoding='utf-8')).hexdigest()
        self.model[TOKEN] = self.answer
        return TOKEN

    def Predict_array(self, BUKET_ID, TEXT_ARRAY):
        pass

    def Get_Result(self, TOKEN):
        return self.model[TOKEN]

    def Clean(self, BUKET_ID):
        pass

    # TODO написать функции логгинга обучения и тестирования качества модели

if __name__ == '__main__':
    pass
