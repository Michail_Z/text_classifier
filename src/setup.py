from setuptools import setup, find_packages
import text_classifier

setup(
    name='text_classifier',
    version=text_classifier.__version__,
    packages=find_packages(),
    long_description='Multi model text classifier',
    install_requires=['pandas', 'numpy', 'scipy', 'scikit-learn'],
    include_package_data=True,
)

