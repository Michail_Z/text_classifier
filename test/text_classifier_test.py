import unittest
import sys
sys.path.insert(0, '../src')

from text_classifier import TextClassifier


class TestTextClassifier(unittest.TestCase):

    def test_Create_buket_correct(self):
        buket = []
        MyModel = 1680
        tc = TextClassifier()
        self.assertEqual(tc.Create_buket(buket, MyModel, 'LR'), 0)

    def test_Create_buket_model_exist(self):
        buket = []
        MyModel = 1680
        tc = TextClassifier()
        tc.Create_buket(buket, MyModel, 'LR')
        self.assertEqual(tc.Create_buket(buket, MyModel, 'LR'), 1)

    def test_Create_buket_model_wrong(self):
        buket = []
        MyModel = 1680
        tc = TextClassifier()
        self.assertEqual(tc.Create_buket(buket, MyModel, 'SVF'), 2)

    def test_Train_the_model_Not_existed(self):
        buket = []
        MyModel = 1680
        tc = TextClassifier()
        tc.Create_buket(buket, MyModel, 'LR')
        self.assertEqual(tc.Train_the_model(MyModel, 'test.csv'), -1)

    def test_Train_is_done_model_clean(self):
        tc = TextClassifier()
        self.assertEqual(tc.Train_is_done(1234), 2)

    def test_Predict_Token(self):
        import hashlib as hl
        buket = []
        MyModel = 1680
        tc = TextClassifier()
        tc.Create_buket(buket, MyModel, 'LR')
        tc.Train_the_model(MyModel, '../learm_ck.csv')
        self.assertEqual(tc.Predict(MyModel, 'Роза Свет Тепло'),
                         hl.sha224(bytes('Роза Свет Тепло',
                                         encoding='utf-8')).
                         hexdigest())


if __name__ == '__main__':
    unittest.main()
